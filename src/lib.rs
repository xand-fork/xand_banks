#![forbid(unsafe_code)]
#![cfg_attr(test, allow(non_snake_case))]

extern crate chrono;
extern crate derive_new;
#[macro_use]
extern crate serde;
#[cfg(test)]
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate tpfs_logger_port;

pub mod banks;
pub mod date_range;
pub mod errors;
pub mod models;
pub mod utils;

mod logging_events;
pub(crate) mod metrics;

pub mod constants;
#[cfg(test)]
mod mocks;

// Re-export money crate so users have a consistent way to use the correct version
pub use xand_money;

use crate::models::account::{DetailedAccount, TransferRequest};
use crate::{
    date_range::DateRange,
    models::{BankBalance, BankTransaction, BankTransferResponse},
};
use async_trait::async_trait;
use std::collections::HashMap;
use std::sync::Arc;
use xand_secrets::SecretKeyValueStore;

/// This represents the minimum amount of base 58 encoded characters required to safely
/// store the pending create/redeem identifier in the bank transfer's memo field. Banks
/// under this limit cannot be used.
///
/// TODO: This may be adjusted pending https://www.pivotaltracker.com/story/show/170057814
pub static MEMO_CHARACTERS_MINIMUM: u32 = 32;
pub type Result<T> = std::result::Result<T, errors::XandBanksErrors>;

/// Banks must implement this Bank trait so that they can be usable by the BankDispatcher.
#[async_trait]
pub trait Bank: dyn_clone::DynClone + Send + Sync {
    /// Returns the routing number of the bank
    fn routing_number(&self) -> String;

    /// Return the reserve account id associated with this bank.
    fn reserve_account(&self) -> String;

    /// Return all member accounts associated with this bank.
    fn accounts(&self) -> Vec<DetailedAccount>;

    /// Returns information about an account at this bank.
    fn get_enriched_account(&self, account_id: &str) -> Result<DetailedAccount>;

    /// Return this bank's display name.
    fn display_name(&self) -> String;

    /// The following methods are just used to abstract out the inner workings of the bank adapters.
    fn memo_char_limit(&self) -> u32;

    async fn balance(&self, account_number: &str) -> Result<BankBalance>;

    async fn transfer(
        &self,
        request: TransferRequest,
        metadata: Option<String>,
    ) -> Result<BankTransferResponse>;

    async fn history(
        &self,
        account_number: &str,
        date_range: DateRange,
    ) -> Result<Vec<BankTransaction>>;
}
dyn_clone::clone_trait_object!(Bank);

/// Operations that must exist for each bank adapter
#[async_trait]
pub trait BankAdapter: dyn_clone::DynClone + Send + Sync {
    /// Given an account number, return the account balance.
    async fn balance(&self, account_number: &str) -> Result<BankBalance>;

    /// Given a source account number, destination account id, and an amount transfer that
    /// amount (in dollars) from source to destination.
    /// Optionally include a metadata String field to attach to the transfer (useful when intending
    /// to be able to match this transfer later.)
    async fn transfer(
        &self,
        request: TransferRequest,
        metadata: Option<String>,
    ) -> Result<BankTransferResponse>;

    /// Given an account number, return the history for that account.
    async fn history(
        &self,
        account_number: &str,
        date_range: DateRange,
    ) -> Result<Vec<BankTransaction>>;

    /// Returns the maximum number of characters that the memo field we must use to correlate
    /// creates and redeems can hold. This is primarily used to fail tests early if the
    /// bank you are implementing won't be able to hold enough characters to prevent collision
    /// attacks. The bank is expected to support the base58 character set for this purpose.
    fn memo_char_limit(&self) -> u32;
}
dyn_clone::clone_trait_object!(BankAdapter);

/// BanksConfig that can be turned into a mapping of Bank implementors for the XandBankDispatcher
/// to operate over.
pub trait BanksConfig {
    /// The keys for this returned map will be used by the XandBankDispatcher to identify the
    /// specific Bank.
    /// This can be bank name, routing number, or any other id that makes sense for the consumer.
    fn get_banks(
        &self,
        secret_store: Arc<dyn SecretKeyValueStore>,
    ) -> Result<HashMap<String, Box<dyn Bank>>>;
}
