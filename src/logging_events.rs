use crate::errors::XandBanksErrors;

#[logging_event]
pub enum LoggingEvent {
    BankAPI(XandBanksErrors),
    #[cfg(test)]
    Msg(String),
}
