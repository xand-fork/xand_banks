use xand_money::Usd;

#[derive(Clone, Debug, Deserialize, PartialEq, Eq, Serialize, Default)]
#[serde(rename_all = "kebab-case")]
pub struct Account {
    pub id: String,
    pub short_name: String,
    pub account_number: String,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Eq, Serialize, Default)]
#[serde(rename_all = "kebab-case")]
pub struct DetailedAccount {
    pub id: String,
    pub short_name: String,
    pub account_number: String,
    pub routing_number: String,
    pub bank_name: String,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Eq, Serialize, Default)]
#[serde(rename_all = "kebab-case")]
pub struct TransferRequest {
    pub src_account_number: String,
    pub dst_account_number: String,
    pub amount: Usd,
}

impl TransferRequest {
    pub fn new(src_no: &str, dst_no: &str, amt: Usd) -> Self {
        TransferRequest {
            src_account_number: src_no.to_string(),
            dst_account_number: dst_no.to_string(),
            amount: amt,
        }
    }
}
