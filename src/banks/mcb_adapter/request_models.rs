use super::account::AccountInfo;
use crate::date_range::DateRange;
use xand_money::Usd;

#[derive(Debug)]
pub struct Transfer {
    pub src_acct: AccountInfo,
    pub dest_acct_id: String,
    pub amount: Usd,
    pub metadata: Option<String>,
}

#[derive(Debug, Clone)]
pub struct HistoryParams {
    pub acct: AccountInfo,
    pub date_range: Option<DateRange>,
    pub pagination_params: Option<PaginationParams>,
}

// TODO: Once MCB fixes their end, specify number of records we want per page to be higher than
//  their default 10: https://www.pivotaltracker.com/story/show/168813976
#[derive(Debug, Clone)]
pub struct PaginationParams {
    pub cursor: McbCursor,
}

#[derive(Debug, Clone)]
pub struct McbCursor(pub(crate) String);
