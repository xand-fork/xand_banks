use crate::banks::mcb_adapter::errors::Result;
use crate::banks::mcb_adapter::{
    account::{AccountInfo, McbAccountInfoManager},
    config::RawAccountInfoManagerConfig,
};
use async_trait::async_trait;
use std::sync::Arc;
use xand_secrets::SecretKeyValueStore;

pub struct AccountInfoManager {
    config: RawAccountInfoManagerConfig,
    secret_store: Arc<dyn SecretKeyValueStore>,
}

impl AccountInfoManager {
    pub(crate) fn new(
        config: RawAccountInfoManagerConfig,
        secret_store: Arc<dyn SecretKeyValueStore>,
    ) -> Self {
        Self {
            config,
            secret_store,
        }
    }
}

#[async_trait]
impl McbAccountInfoManager for AccountInfoManager {
    async fn get_account_info(&self, account_id: &str) -> Result<AccountInfo> {
        let client_app_ident = self
            .secret_store
            .read(self.config.secret_key_client_app_ident.as_str())
            .await?;
        let organization_id = self
            .secret_store
            .read(self.config.secret_key_organization_id.as_str())
            .await?;
        Ok(AccountInfo {
            account_id: account_id.to_string(),
            client_app_ident,
            organization_id,
        })
    }
}
