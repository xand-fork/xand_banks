use pseudo::Mock;
use serde::{de::DeserializeOwned, Serialize};

use mcb_auth_gen::models::AccessTokenOutput;

use crate::banks::mcb_adapter::access_token_manager::ClientTokenRetrievalAuthentication;
use crate::banks::mcb_adapter::account::{AccountInfo, McbAccountInfoManager};
use crate::banks::mcb_adapter::client_wrapper::{AuthClientWrapper, DataClientWrapper};
use crate::banks::mcb_adapter::errors::{McbHttpError, Result};
use async_trait::async_trait;
use futures::lock::Mutex;
use mcb_acct_gen::models::{AcctInqRq, AcctInqRs};
use mcb_acct_trn_gen::models::{AcctTrnInqRq, AcctTrnInqRs};
use mcb_transfer_gen::models::{XferReq, XferRes};
use std::collections::VecDeque;
use std::sync::Arc;
use xand_secrets::{ExposeSecret, Secret};

#[derive(Clone)]
pub struct MockAccountInfoManager {}

#[async_trait]
impl McbAccountInfoManager for MockAccountInfoManager {
    async fn get_account_info(&self, account_id: &str) -> Result<AccountInfo> {
        Ok(AccountInfo {
            account_id: account_id.to_string(),
            client_app_ident: Secret::new(String::default()),
            organization_id: Secret::new(String::default()),
        })
    }
}

#[derive(Clone)]
pub struct MockMcbRawClient {
    pub get_auth_token: Mock<ClientTokenRetrievalAuthentication, Result<Arc<AccessTokenOutput>>>,
    pub get_account_balance: Mock<(String, Arc<AcctInqRq>), Result<Arc<AcctInqRs>>>,
    pub transfer: Mock<(String, Arc<XferReq>), Result<Arc<XferRes>>>,
    pub history: Mock<(String, Arc<AcctTrnInqRq>), Result<Arc<AcctTrnInqRs>>>,
    pub history_responses: Arc<Mutex<VecDeque<AcctTrnInqRs>>>,
}

impl Default for MockMcbRawClient {
    fn default() -> Self {
        Self {
            get_auth_token: Mock::new(Err(McbHttpError::Mock {})),
            get_account_balance: Mock::new(Err(McbHttpError::Mock {})),
            transfer: Mock::new(Err(McbHttpError::Mock {})),
            history: Mock::new(Err(McbHttpError::Mock {})),
            history_responses: Arc::new(Mutex::new(VecDeque::new())),
        }
    }
}

impl MockMcbRawClient {
    pub fn set_history_responses_vec(&mut self, responses: Vec<AcctTrnInqRs>) {
        let vec_deque = responses.into();
        self.history_responses = Arc::new(Mutex::new(vec_deque));
    }
}

#[async_trait]
impl DataClientWrapper for MockMcbRawClient {
    async fn account_balance(
        &self,
        token: &Secret<String>,
        acct_info: AcctInqRq,
    ) -> Result<AcctInqRs> {
        self.get_account_balance
            .call((token.expose_secret().clone(), Arc::new(acct_info)))
            .map(serde_copy)
    }

    async fn transfer(&self, token: &Secret<String>, req: XferReq) -> Result<XferRes> {
        self.transfer
            .call((token.expose_secret().clone(), Arc::new(req)))
            .map(serde_copy)
    }

    async fn history(&self, token: &Secret<String>, req: AcctTrnInqRq) -> Result<AcctTrnInqRs> {
        let mock_stored_result = self
            .history
            .call((token.expose_secret().clone(), Arc::new(req)))
            .map(serde_copy);
        let mut history_resp_vec = self.history_responses.lock().await;
        match history_resp_vec.pop_front() {
            None => mock_stored_result,
            Some(val) => Ok(val),
        }
    }
}

#[async_trait]
impl AuthClientWrapper for MockMcbRawClient {
    async fn get_auth_token(
        &self,
        auth: ClientTokenRetrievalAuthentication,
    ) -> Result<AccessTokenOutput> {
        self.get_auth_token.call(auth).map(serde_copy)
    }
}

pub fn serde_copy<T: Serialize + DeserializeOwned, REF: AsRef<T>>(obj_ref: REF) -> T {
    let obj = obj_ref.as_ref();
    let serialized = serde_json::to_string(&obj).unwrap();
    serde_json::from_str(&serialized).unwrap()
}
