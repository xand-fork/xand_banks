use crate::{
    banks::mcb_adapter::{
        access_token_manager::{AccessTokenManager, RefreshPolicy},
        errors::*,
        test::{
            mock_raw_client::serde_copy,
            mock_raw_client::MockAccountInfoManager,
            util::{
                self,
                component_builders::{
                    build_adapter_with, get_default_access_token_manager, get_mock_raw_client,
                },
                component_builders::{get_default_auth_config, get_default_fake_secret_store},
                response_builders::{
                    build_default_balance_resp, build_default_history_resp,
                    build_default_transfer_resp, get_bal_vec_from_resp,
                },
            },
        },
    },
    models::account::TransferRequest,
    models::{BankTransaction, BankTransactionType},
};
use futures::executor::block_on;
use mcb_acct_gen::models::AcctInqRs;
use mcb_auth_gen::models::AccessTokenOutput;
use pseudo::Mock;
use std::convert::TryFrom;
use std::sync::Arc;
use xand_money::{Money, Usd};
use xand_secrets::{ExposeSecret, Secret};

#[test]
fn can_construct_with_mock_raw_client() {
    let mock_mcb_client = get_mock_raw_client();
    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let _mcb_adapter =
        build_adapter_with(mock_mcb_client, token_manager, mock_account_info_manager);
}

#[test]
fn client_can_fetch_and_store_token() {
    let mock_mcb_client = get_mock_raw_client();
    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(
        mock_mcb_client.clone(),
        token_manager,
        mock_account_info_manager,
    );

    let token = block_on(mcb_adapter.get_token()).unwrap();
    let expected_token = Secret::new("secretToken".to_string());

    assert_eq!(token.expose_secret(), expected_token.expose_secret());
    assert_eq!(mock_mcb_client.get_auth_token.num_calls(), 1);

    // Assert that calling get_token() again does not change the value, and the underlying http client
    // doesn't get invoked again
    let next_token = block_on(mcb_adapter.get_token()).unwrap();
    assert_eq!(next_token.expose_secret(), expected_token.expose_secret());
    assert_eq!(mock_mcb_client.get_auth_token.num_calls(), 1);
}

#[test]
fn client_errors_if_negative_expiring_token() {
    // Setup non-sensical expiration time
    let mut token_response = AccessTokenOutput::new();
    token_response.access_token = Some("secretToken".to_string());
    token_response.expires_in = Some(-10); // Invalid negative expiry
    token_response.token_type = Some("Bearer".to_string());
    let token_response: Arc<_> = Arc::new(token_response);

    // Set up mock adapter and mock response
    let mock_mcb_client = get_mock_raw_client();
    mock_mcb_client
        .get_auth_token
        .return_value(Ok(token_response));
    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(mock_mcb_client, token_manager, mock_account_info_manager);

    // Query for first token, that will "expire" in 60s
    let token_resp = block_on(mcb_adapter.get_token());

    // Get a Display of the result in case we need to print it on test failure
    let err_msg = format!("{:?}", token_resp);
    if let McbHttpError::CastError { .. } =
        token_resp.expect_err(&format!("Call should error. Found: {:?}", &err_msg))
    {
        // pass
    } else {
        panic!("Wrong error type returned. Found {:?}", &err_msg);
    }
}

#[test]
fn client_fetches_token_again_if_in_expiry_window() {
    // Construct first and second response for the mock get_token() queries
    let mut mock_mcb_client = get_mock_raw_client();

    // Note: First token response will "expire" in 60s
    let mut token_response = AccessTokenOutput::new();
    token_response.access_token = Some("secretToken".to_string());
    token_response.expires_in = Some(util::component_builders::TOKEN_EXPIRATION_TIME);
    token_response.token_type = Some("Bearer".to_string());
    let token_response: Arc<_> = Arc::new(token_response);

    let mut second_token_response = serde_copy(token_response.clone());
    second_token_response.access_token = Some("secondSecretToken".to_string());

    let mock_response = Mock::new(Ok(token_response));
    mock_mcb_client.get_auth_token = mock_response;

    // Specify explicity refresh policy
    let policy =
        RefreshPolicy::PriorToTokenExpiration(util::component_builders::POLICY_REFRESH_TIME);
    let token_manager = AccessTokenManager::new(
        policy,
        get_default_auth_config(),
        get_default_fake_secret_store(),
        mock_mcb_client.clone(),
    );

    // Build the McbAdapter
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(
        mock_mcb_client.clone(),
        token_manager,
        mock_account_info_manager,
    );

    // Query for first token, that will "expire" in 60s
    let token = block_on(mcb_adapter.get_token()).unwrap();
    let expected_token = Secret::new("secretToken".to_string());

    assert_eq!(token.expose_secret(), expected_token.expose_secret());
    assert_eq!(mock_mcb_client.get_auth_token.num_calls(), 1);

    // Assert that calling get_token() when in expiry window returns a new token, and underlying client is invoked.
    mock_mcb_client
        .get_auth_token
        .return_value(Ok(Arc::new(second_token_response)));
    let next_token = block_on(mcb_adapter.get_token()).unwrap();
    let expected_next_token = Secret::new("secondSecretToken".to_string());
    assert_eq!(
        next_token.expose_secret(),
        expected_next_token.expose_secret()
    );
    assert_eq!(mock_mcb_client.get_auth_token.num_calls(), 2);
}

#[test]
fn client_can_fetch_balance() {
    // Build mock balance response
    let cur_bal = "100.25".to_string();
    let avail_bal = "99.25".to_string();
    let balance_resp = build_default_balance_resp(cur_bal, avail_bal);
    let mock_balance_resp = Ok(Arc::new(balance_resp));

    // Build mock MCBAdapter
    let mock_mcb_client = get_mock_raw_client();
    mock_mcb_client
        .get_account_balance
        .return_value(mock_balance_resp);
    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(mock_mcb_client, token_manager, mock_account_info_manager);

    let account_no = "111111";
    let balance = block_on(mcb_adapter.get_balance(account_no)).unwrap();

    // Assert adapter has received and parsed the expected values
    assert_eq!(f64::try_from(balance.current_balance).unwrap(), 100.25_f64);
    assert_eq!(f64::try_from(balance.available_balance).unwrap(), 99.25_f64);
}

#[test]
fn client_fetches_balance_with_correct_token() {
    // Build mock balance response
    let cur_bal = "100.25".to_string();
    let avail_bal = "99.25".to_string();
    let balance_resp = build_default_balance_resp(cur_bal, avail_bal);
    let mock_balance_resp = Ok(Arc::new(balance_resp));

    // Build mock MCBAdapter
    let mock_mcb_client = get_mock_raw_client();
    mock_mcb_client
        .get_account_balance
        .return_value(mock_balance_resp);

    let token_response = Arc::new(AccessTokenOutput {
        access_token: Some("secretToken".to_string()),
        expires_in: Some(util::component_builders::TOKEN_EXPIRATION_TIME),
        token_type: Some("Bearer".to_string()),
    });
    mock_mcb_client.get_auth_token.return_ok(token_response);

    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(
        mock_mcb_client.clone(),
        token_manager,
        mock_account_info_manager,
    );

    let account_no = "111111";
    let _balance = block_on(mcb_adapter.get_balance(account_no)).unwrap();

    assert!(mock_mcb_client.get_account_balance.called());
    assert!(mock_mcb_client
        .get_account_balance
        .calls()
        .iter()
        .all(|(token, _)| token == "secretToken"));
}

#[test]
fn client_can_fetch_negative_current_balance() {
    // Build mock balance response
    let cur_bal = "-30.00".to_string();
    let avail_bal = "99.25".to_string();
    let balance_resp = build_default_balance_resp(cur_bal, avail_bal);
    let mock_balance_resp = Ok(Arc::new(balance_resp));

    // Build mock MCBAdapter
    let mock_mcb_client = get_mock_raw_client();
    mock_mcb_client
        .get_account_balance
        .return_value(mock_balance_resp);
    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(mock_mcb_client, token_manager, mock_account_info_manager);

    let account_no = "111111";
    let balance = block_on(mcb_adapter.get_balance(account_no)).unwrap();

    // Assert adapter has received and parsed the expected values
    assert_eq!(f64::try_from(balance.current_balance).unwrap(), -30.00_f64);
    assert_eq!(f64::try_from(balance.available_balance).unwrap(), 99.25_f64);
}

#[test]
fn client_errors_if_cant_parse_balance() {
    // Build mock balance response
    let cur_bal = "100.25".to_string();
    let avail_bal = "notARealNumber".to_string();
    let balance_resp = build_default_balance_resp(cur_bal, avail_bal);
    let mock_balance_resp = Ok(Arc::new(balance_resp));

    // Build mock MCBAdapter
    let mock_mcb_client = get_mock_raw_client();
    mock_mcb_client
        .get_account_balance
        .return_value(mock_balance_resp);
    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(mock_mcb_client, token_manager, mock_account_info_manager);

    let account_no = "111111";
    let balance = block_on(mcb_adapter.get_balance(account_no));

    // Get a Display of the result in case we need to print it on test failure
    let err_msg = format!("{:?}", balance);
    if let McbHttpError::MoneyError { .. } =
        balance.expect_err(&format!("Call should error. Found: {:?}", &err_msg))
    {
        // pass
    } else {
        panic!("Wrong error type returned. Found {:?}", &err_msg);
    }
}

#[test]
fn client_errors_if_missing_balance_vec() {
    // Build mock balance response
    let cur_bal = "100.25".to_string();
    let avail_bal = "99.25".to_string();
    let mut balance_resp = build_default_balance_resp(cur_bal, avail_bal);

    // Set entire balance vec to None
    let maybe_bal_vec = get_bal_vec_from_resp(&mut balance_resp);
    *maybe_bal_vec = None;

    assert_incomplete_balance_response(balance_resp);
}

#[test]
fn client_errors_if_incorrect_balance_tags() {
    // Build invalid mock balance response
    let cur_bal = "100.25".to_string();
    let avail_bal = "99.25".to_string();
    let mut balance_resp = build_default_balance_resp(cur_bal, avail_bal);

    // Get the balance vec
    let maybe_bal_vec = get_bal_vec_from_resp(&mut balance_resp);
    let bal_vec = maybe_bal_vec.as_mut().unwrap();

    // Change all tags to "invalidtag"
    bal_vec.iter_mut().for_each(|balance_entry| {
        balance_entry.bal_type.as_mut().unwrap().bal_type_source = Some("invalidtag".to_string())
    });
    assert_incomplete_balance_response(balance_resp);
}

#[test]
fn client_can_submit_transfer() {
    // Build mock transfer response
    let src_acct = "1111111111";
    let status = "Valid";
    let confirmation_number = "848494";

    let transfer_resp = build_default_transfer_resp(src_acct, status, confirmation_number);
    let mock_transfer_resp = Ok(Arc::new(transfer_resp));

    // Build mock MCBAdapter
    let mock_mcb_client = get_mock_raw_client();
    mock_mcb_client.transfer.return_value(mock_transfer_resp);
    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(mock_mcb_client, token_manager, mock_account_info_manager);

    // Submit transfer request
    let dest_acct = "2222222222";
    let amt = Usd::from_f64_major_units(725).unwrap();
    let metadata = Some("doing a cool transfer".to_string());

    let resp =
        block_on(mcb_adapter.transfer(TransferRequest::new(src_acct, dest_acct, amt), metadata))
            .unwrap();

    // Assert adapter has received and parsed the expected values
    assert_eq!(resp.message, status);
}

#[test]
fn client_submits_transfers_with_authentication() {
    // Build mock transfer response
    let src_acct = "1111111111";
    let status = "Valid";
    let confirmation_number = "848494";

    let transfer_resp = build_default_transfer_resp(src_acct, status, confirmation_number);
    let mock_transfer_resp = Ok(Arc::new(transfer_resp));

    // Build mock MCBAdapter
    let mock_mcb_client = get_mock_raw_client();
    mock_mcb_client.transfer.return_value(mock_transfer_resp);

    let token_response = Arc::new(AccessTokenOutput {
        access_token: Some("secretToken".to_string()),
        expires_in: Some(util::component_builders::TOKEN_EXPIRATION_TIME),
        token_type: Some("Bearer".to_string()),
    });
    mock_mcb_client.get_auth_token.return_ok(token_response);

    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(
        mock_mcb_client.clone(),
        token_manager,
        mock_account_info_manager,
    );

    // Submit transfer request
    let dest_acct = "2222222222";
    let amt = Usd::from_f64_major_units(725).unwrap();
    let metadata = Some("doing a cool transfer".to_string());

    let _resp =
        block_on(mcb_adapter.transfer(TransferRequest::new(src_acct, dest_acct, amt), metadata))
            .unwrap();

    assert!(mock_mcb_client.transfer.called());
    assert!(mock_mcb_client
        .transfer
        .calls()
        .iter()
        .all(|(token, _)| token == "secretToken"));
}

#[test]
fn client_can_get_history_1_page() {
    // Build mock history response
    let acct = "1111111111";
    let test_txn_vec: Vec<BankTransaction> = vec![
        BankTransaction {
            bank_unique_id: String::from("1234"),
            amount: Usd::from_i64_minor_units(1025).unwrap(),
            metadata: "metadata1".to_string(),
            txn_type: BankTransactionType::Credit,
        },
        BankTransaction {
            bank_unique_id: String::from("5678"),
            amount: Usd::from_i64_minor_units(1125).unwrap(),
            metadata: "metadata2".to_string(),
            txn_type: BankTransactionType::Debit,
        },
    ];

    let history_resp = build_default_history_resp(acct, test_txn_vec, None);
    let mock_history_resp = Ok(Arc::new(history_resp));

    // Build mock MCBAdapter
    let mock_mcb_client = get_mock_raw_client();
    mock_mcb_client.history.return_value(mock_history_resp);
    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(mock_mcb_client, token_manager, mock_account_info_manager);

    // Submit history request
    let history_resp = block_on(mcb_adapter.history(acct, None)).unwrap();

    // Assert adapter has received and parsed the expected values
    assert_eq!(history_resp.len(), 2);
    assert_eq!(
        history_resp[0].amount,
        Usd::from_i64_minor_units(1025).unwrap()
    );
    assert_eq!(history_resp[0].metadata, "metadata1");
    assert_eq!(history_resp[0].txn_type, BankTransactionType::Credit);
    assert_eq!(history_resp[0].bank_unique_id, "1234");

    assert_eq!(
        history_resp[1].amount,
        Usd::from_i64_minor_units(1125).unwrap()
    );
    assert_eq!(history_resp[1].metadata, "metadata2");
    assert_eq!(history_resp[1].txn_type, BankTransactionType::Debit);
    assert_eq!(history_resp[1].bank_unique_id, "5678");
}

#[test]
fn client_passes_correct_secret_token_when_fetching_history() {
    // Build mock history response
    let acct = "1111111111";
    let test_txn_vec: Vec<BankTransaction> = vec![
        BankTransaction {
            bank_unique_id: String::from("1234"),
            amount: Usd::from_i64_minor_units(1025).unwrap(),
            metadata: "metadata1".to_string(),
            txn_type: BankTransactionType::Credit,
        },
        BankTransaction {
            bank_unique_id: String::from("5678"),
            amount: Usd::from_i64_minor_units(1125).unwrap(),
            metadata: "metadata2".to_string(),
            txn_type: BankTransactionType::Debit,
        },
    ];

    let history_resp = build_default_history_resp(acct, test_txn_vec, None);
    let mock_history_resp = Ok(Arc::new(history_resp));

    // Build mock MCBAdapter
    let mock_mcb_client = get_mock_raw_client();
    mock_mcb_client.history.return_value(mock_history_resp);

    let token_response = Arc::new(AccessTokenOutput {
        access_token: Some("secretToken".to_string()),
        expires_in: Some(util::component_builders::TOKEN_EXPIRATION_TIME),
        token_type: Some("Bearer".to_string()),
    });
    mock_mcb_client.get_auth_token.return_ok(token_response);

    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(
        mock_mcb_client.clone(),
        token_manager,
        mock_account_info_manager,
    );

    // Submit history request
    let _history_resp = block_on(mcb_adapter.history(acct, None)).unwrap();

    assert!(mock_mcb_client.history.called());
    assert!(mock_mcb_client
        .history
        .calls()
        .iter()
        .all(|(token, _)| token == "secretToken"));
}

/// Assert that client calls for next page if cursor is present
#[test]
fn client_get_history_continues_if_cursor_is_some() {
    // Build mock history response
    let acct = "1111111111";
    let test_txn_vec: Vec<BankTransaction> = vec![
        BankTransaction {
            bank_unique_id: String::from("1234"),
            amount: Usd::from_i64_minor_units(1025).unwrap(),
            metadata: "metadata1".to_string(),
            txn_type: BankTransactionType::Debit,
        },
        BankTransaction {
            bank_unique_id: String::from("5678"),
            amount: Usd::from_i64_minor_units(1125).unwrap(),
            metadata: "metadata2".to_string(),
            txn_type: BankTransactionType::Credit,
        },
    ];

    // Set up mock to return the 2 differnt responses for paginated history. First with
    // cursor, next response is without
    let test_cursor = "1237858";
    let history_resp_1 = build_default_history_resp(acct, test_txn_vec.clone(), Some(test_cursor));
    let history_resp_2 = build_default_history_resp(acct, test_txn_vec, None);
    let history_resp_vec = vec![history_resp_1, history_resp_2];
    let mut mock_mcb_client = get_mock_raw_client();
    mock_mcb_client.set_history_responses_vec(history_resp_vec);

    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(
        mock_mcb_client.clone(),
        token_manager,
        mock_account_info_manager,
    );

    // Submit history request
    let history_resp = block_on(mcb_adapter.history(acct, None)).unwrap();

    // Assert adapter calls the raw client twice, for the 2 loaded "pages"
    let num_raw_hist_calls = mock_mcb_client.history.num_calls();
    assert_eq!(num_raw_hist_calls, 2);
    assert_eq!(history_resp.len(), 4);
}

/// Util assert method for invalid balance responses
fn assert_incomplete_balance_response(balance_resp: AcctInqRs) {
    let mock_balance_resp = Ok(Arc::new(balance_resp));

    // Build mock MCBAdapter
    let mock_mcb_client = get_mock_raw_client();
    mock_mcb_client
        .get_account_balance
        .return_value(mock_balance_resp);
    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(mock_mcb_client, token_manager, mock_account_info_manager);

    let account_no = "111111";
    let balance = block_on(mcb_adapter.get_balance(account_no));

    // Get a Display of the result in case we need to print it on test failure
    let err_msg = format!("{:?}", balance);
    if let McbHttpError::IncompleteMcbHttpResponseBody { .. } =
        balance.expect_err(&format!("Call should error. Found: {:?}", &err_msg))
    {
        // pass
    } else {
        panic!("Wrong error type returned. Found {:?}", &err_msg);
    }
}
