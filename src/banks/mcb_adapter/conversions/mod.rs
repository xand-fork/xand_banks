use crate::banks::mcb_adapter::conversions::extensions::balance::AcctInqRqExt;
use crate::banks::mcb_adapter::conversions::extensions::history::AcctTrnInqRqExt;
use crate::banks::mcb_adapter::conversions::extensions::transfer::XferRqExt;
use crate::banks::mcb_adapter::transaction::McbTransactionType;
use crate::{
    banks::mcb_adapter::conversions::extensions::balance::AcctInqRsExt,
    banks::mcb_adapter::conversions::extensions::history::AcctTrnRecExt,
    banks::mcb_adapter::conversions::extensions::transfer::XferResExt,
    banks::mcb_adapter::transaction::McbTransaction,
    banks::mcb_adapter::{
        account::AccountInfo,
        conversions::util::balance,
        errors::*,
        request_models::{HistoryParams, Transfer},
    },
    models::{BankBalance, BankTransaction, BankTransactionType, BankTransferResponse},
};
use mcb_acct_gen::models::{AcctInqRq, AcctInqRs};
use mcb_acct_trn_gen::models::{AcctTrnInqRq, AcctTrnRec};
use mcb_transfer_gen::models::{XferReq, XferRes};
use std::convert::TryFrom;

pub mod extensions;
mod util;

/// Enum to differentiate available and current balances; we ignore other possible balances
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum BalTypeSource {
    Avail,
    Current,
}

/// Convert from Domain -> MCB http types
impl TryFrom<AccountInfo> for AcctInqRq {
    type Error = McbHttpError;

    fn try_from(acct_info: AccountInfo) -> Result<Self> {
        let req = AcctInqRq::fill_with(acct_info);
        match req {
            Ok(strct) => Ok(*strct),
            Err(err) => Err(err),
        }
    }
}

/// Convert from MCB http types -> Domain
impl TryFrom<AcctInqRs> for BankBalance {
    type Error = McbHttpError;

    fn try_from(acct_inq_rs: AcctInqRs) -> Result<Self> {
        let acct_bal_vec = acct_inq_rs.get_balance_vec()?;

        // Search and parse for available and current balances
        let avail_bal = balance::get_balance_where_src_eq(&acct_bal_vec, &BalTypeSource::Avail)?;
        let cur_bal = balance::get_balance_where_src_eq(&acct_bal_vec, &BalTypeSource::Current)?;

        Ok(BankBalance {
            available_balance: avail_bal,
            current_balance: cur_bal,
        })
    }
}

impl TryFrom<Transfer> for XferReq {
    type Error = McbHttpError;

    fn try_from(transfer: Transfer) -> Result<Self> {
        let req = XferReq::fill_with(transfer);
        match req {
            Ok(strct) => Ok(*strct),
            Err(err) => Err(err),
        }
    }
}

impl TryFrom<XferRes> for BankTransferResponse {
    type Error = McbHttpError;

    fn try_from(res: XferRes) -> Result<Self> {
        let message = res.get_transfer_response()?;

        Ok(BankTransferResponse { message })
    }
}

/// Use to convert HistoryParams into the the AccountTransactionInquiryRequest object. Specifies
/// a null cursor
impl TryFrom<&HistoryParams> for AcctTrnInqRq {
    type Error = McbHttpError;

    fn try_from(history_params: &HistoryParams) -> Result<Self> {
        let req = AcctTrnInqRq::fill_with(history_params);
        match req {
            Ok(strct) => Ok(*strct),
            Err(err) => Err(err),
        }
    }
}

impl TryFrom<&AcctTrnRec> for McbTransaction {
    type Error = McbHttpError;

    fn try_from(rec: &AcctTrnRec) -> Result<Self> {
        let (txn_status, effect_datetime) = rec.get_txn_status_and_effective_datetime()?;

        Ok(Self {
            unique_id: rec.get_txn_id()?,
            amount: rec.get_txn_amount()?,
            metadata: rec.get_txn_metadata()?,
            txn_type: rec.get_txn_type()?,
            txn_date: rec.get_txn_date()?,
            txn_status,
            effect_datetime,
        })
    }
}

impl From<McbTransaction> for BankTransaction {
    fn from(mcb_txn: McbTransaction) -> Self {
        let b_txn_type = match mcb_txn.txn_type {
            McbTransactionType::Debit => BankTransactionType::Debit,
            McbTransactionType::Credit => BankTransactionType::Credit,
        };

        BankTransaction {
            bank_unique_id: mcb_txn.unique_id,
            amount: mcb_txn.amount,
            metadata: mcb_txn.metadata,
            txn_type: b_txn_type,
        }
    }
}
