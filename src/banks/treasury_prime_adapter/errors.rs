use crate::banks::errors::BankErrors;
use crate::errors::XandBanksErrors;
use reqwest::Error;
use snafu::Snafu;
use std::sync::Arc;
use xand_money::MoneyError as XandMoneyError;
use xand_secrets::ReadSecretError;

pub type Result<T> = std::result::Result<T, TreasuryPrimeError>;

#[derive(Clone, Debug, Serialize, Snafu)]
#[snafu(visibility(pub))]
pub enum TreasuryPrimeError {
    #[snafu(display("TreasuryPrimeClientError: {}", message))]
    TreasuryPrimeClientError { message: String },
    #[snafu(display("Unable to find account. Account number: {}", account_number))]
    AccountNotFound { account_number: String },
    #[snafu(display("Retrieved more than one account. Account number: {}", account_number))]
    MoreThanOneAccount { account_number: String },
    #[snafu(display(
        "Missing transfer metadata. From account number: {}, To account number: {}",
        src_acct_no,
        dest_acct_no
    ))]
    RequestMissingMetadata {
        src_acct_no: String,
        dest_acct_no: String,
    },
    #[snafu(display("Unable to parse next page cursor URL: {}", url))]
    UrlParseError { url: String },
    #[snafu(display("Problem converting Money: {}", source))]
    MoneyError {
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: XandMoneyError,
    },
    #[snafu(display("Secret store lookup error: {}", source))]
    SecretStoreLookupError {
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<ReadSecretError>,
    },
    #[snafu(display("Reqwest error: {}", message))]
    ReqwestError { message: String },
}

impl From<XandMoneyError> for TreasuryPrimeError {
    fn from(source: XandMoneyError) -> Self {
        Self::MoneyError { source }
    }
}

// TODO - may want to do this more idiomatically vis-a-vis the existing error conversions in this crate
impl From<XandMoneyError> for XandBanksErrors {
    fn from(source: XandMoneyError) -> Self {
        XandBanksErrors::BankError {
            source: BankErrors::TreasuryPrime {
                source: TreasuryPrimeError::MoneyError { source },
            },
        }
    }
}

impl From<ReadSecretError> for TreasuryPrimeError {
    fn from(source: ReadSecretError) -> Self {
        Self::SecretStoreLookupError {
            source: Arc::new(source),
        }
    }
}

impl From<reqwest::Error> for TreasuryPrimeError {
    fn from(reqwest_error: Error) -> Self {
        Self::ReqwestError {
            message: reqwest_error.to_string(),
        }
    }
}
