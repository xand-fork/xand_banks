use crate::banks::treasury_prime_adapter::{client_wrapper::RealClient, TreasuryPrimeAdapter};
use crate::Result;
use url::Url;

use super::{super::config::BankConfigurationErrors, client_wrapper::ClientConfiguration};
use crate::utils::trim_trailing_slash;
use std::sync::Arc;
use std::time::Duration;
use xand_secrets::SecretKeyValueStore;

#[derive(Serialize, Deserialize, Eq, PartialEq, Clone, Debug, Ord, PartialOrd, Hash)]
#[serde(rename_all = "kebab-case")]
pub struct TreasuryPrimeConfig {
    #[serde(with = "url_serde")]
    pub url: Url,
    pub auth: TreasuryPrimeAuthConfig,
    pub timeout: u64,
}

#[derive(Serialize, Deserialize, Eq, PartialEq, Clone, Debug, Ord, PartialOrd, Hash)]
#[serde(rename_all = "kebab-case")]
pub struct TreasuryPrimeAuthConfig {
    pub secret_key_username: String,
    pub secret_key_password: String,
}

impl TreasuryPrimeConfig {
    pub fn get_adapter(
        &self,
        secret_store: Arc<dyn SecretKeyValueStore>,
    ) -> Result<TreasuryPrimeAdapter<RealClient>> {
        TreasuryPrimeAdapter::<RealClient>::new(self, secret_store)
    }

    pub fn treasury_prime_client_config(&self) -> Result<ClientConfiguration> {
        Ok(ClientConfiguration {
            base_path: trim_trailing_slash(self.url.to_string()),
            user_agent: None,
            timeout: Duration::from_secs(self.timeout),
        })
    }

    pub fn validate(&self) -> Result<()> {
        let TreasuryPrimeAuthConfig {
            secret_key_username,
            secret_key_password,
        } = &self.auth;

        if secret_key_username.trim().is_empty() {
            return Err(BankConfigurationErrors::GenericConfigError {
                msg: "Treasury prime secret_key_username cannot be blank"
                    .parse()
                    .unwrap(),
            }
            .into());
        }

        if secret_key_password.trim().is_empty() {
            return Err(BankConfigurationErrors::GenericConfigError {
                msg: "Treasury prime secret_key_password cannot be blank"
                    .parse()
                    .unwrap(),
            }
            .into());
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::constants::BANK_CALL_STANDARD_TIMEOUT;

    #[test]
    fn api_username_secret_store_lookup_key_cannot_be_blank() {
        let config = TreasuryPrimeConfig {
            url: Url::parse("http://localhost").unwrap(),
            auth: TreasuryPrimeAuthConfig {
                secret_key_username: String::from(""),
                secret_key_password: String::from("fake/secrets/path/api_key_value"),
            },
            timeout: BANK_CALL_STANDARD_TIMEOUT,
        };

        let result = config.validate();
        assert!(result.is_err(), "{:?}", result);

        assert_eq!(
            result.unwrap_err().to_string(),
            "Configuration issue: Treasury prime secret_key_username cannot be blank",
        );
    }

    #[test]
    fn api_password_secret_store_lookup_key_cannot_be_blank() {
        let config = TreasuryPrimeConfig {
            url: Url::parse("http://localhost").unwrap(),
            auth: TreasuryPrimeAuthConfig {
                secret_key_username: String::from("fake/secrets/path/api_key_id"),
                secret_key_password: String::from(""),
            },
            timeout: BANK_CALL_STANDARD_TIMEOUT,
        };

        let result = config.validate();
        assert!(result.is_err(), "{:?}", result);

        assert_eq!(
            result.unwrap_err().to_string(),
            "Configuration issue: Treasury prime secret_key_password cannot be blank",
        );
    }

    #[test]
    fn client_config_created_correctly_from_config() {
        let config = TreasuryPrimeConfig {
            url: Url::parse("http://localhost").unwrap(),
            auth: TreasuryPrimeAuthConfig {
                secret_key_username: String::from("fake/secrets/path/api_key_id"),
                secret_key_password: String::from("fake/secrets/path/api_key_value"),
            },
            timeout: BANK_CALL_STANDARD_TIMEOUT,
        };

        let adapter_config = config.treasury_prime_client_config().unwrap();

        assert_eq!(adapter_config.base_path, "http://localhost".to_string());
    }
}
