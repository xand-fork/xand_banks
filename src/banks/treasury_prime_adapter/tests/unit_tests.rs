use crate::banks::treasury_prime_adapter::errors::TreasuryPrimeError;
use crate::banks::treasury_prime_adapter::errors::TreasuryPrimeError::TreasuryPrimeClientError;
use crate::banks::treasury_prime_adapter::test_utils::fake_wrapper::{
    BookTransferPage, GetBookTransferRequest, Transfer,
};
use crate::banks::treasury_prime_adapter::test_utils::{
    get_basic_http_client_authentication, get_default_client_authentication, TreasuryPrimeScenario,
};
use crate::mocks::fake_secret_store::FakeSecretStore;
use crate::models::account::TransferRequest;
use xand_money::{Money, Usd};

const SENT_STATUS: &str = "sent";

// TODO move unit tests into tests folder

#[tokio::test]
async fn balance_returns_balances_from_given_account() {
    TreasuryPrimeScenario::default()
        .given_account_number_with_balances("account1", "5.04", "5.05")
        .when_i_get_balance_for_account("account1")
        .await
        .then_available_balance_is(Usd::from_f64_major_units(5.04_f64).unwrap())
        .then_current_balance_is(Usd::from_f64_major_units(5.05_f64).unwrap())
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn when_account_detail_has_error_bank_returns_xand_bank_error() {
    TreasuryPrimeScenario::default()
        .given_account_number_with_balances("account1", "5.04", "5.05")
        .given_account_details_throws_error(TreasuryPrimeError::TreasuryPrimeClientError {
            message: "some server error!".to_string(),
        })
        .when_i_get_balance_for_account("account1")
        .await
        .then_balance_returned_error_message("TreasuryPrimeClientError: some server error!")
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn when_accounts_unreachable_then_error_returned() {
    TreasuryPrimeScenario::default()
        .given_account_summaries_for_account_number_throws_error(
            "account1",
            TreasuryPrimeError::TreasuryPrimeClientError {
                message: "some server error!".to_string(),
            },
        )
        .when_i_get_balance_for_account("account1")
        .await
        .then_balance_returned_error_message("TreasuryPrimeClientError: some server error!")
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn when_available_balance_cannot_be_parsed_to_f64_then_format_error() {
    TreasuryPrimeScenario::default()
        .given_account_number_with_balances("account1", "hello", "5.05")
        .when_i_get_balance_for_account("account1")
        .await
        .then_balance_returned_error()
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn when_current_balance_cannot_be_parsed_to_f64_then_format_error() {
    TreasuryPrimeScenario::default()
        .given_account_number_with_balances("account1", "5.04", "hello")
        .when_i_get_balance_for_account("account1")
        .await
        .then_balance_returned_error()
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn nonexistent_account_returns_error() {
    TreasuryPrimeScenario::default()
        .given_there_are_no_accounts()
        .when_i_get_balance_for_account("account1")
        .await
        .then_balance_returned_error_message("Unable to find account. Account number: account1")
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn more_than_one_account_per_account_number_returns_error() {
    TreasuryPrimeScenario::default()
        .given_two_accounts_with_same_account_number("account1")
        .when_i_get_balance_for_account("account1")
        .await
        .then_balance_returned_error_message(
            "Retrieved more than one account. Account number: account1",
        )
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn transfer_creates_whole_number_values_with_decimals() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_account_number_maps_to_account_id("account2", "account2_id")
        .when_i_transfer_from_to_accounts(
            TransferRequest::new(
                "account1",
                "account2",
                Usd::from_f64_major_units(3.00_f64).unwrap(),
            ),
            Some("some random metadata".to_string()),
        )
        .await
        .then_book_transfer_amount_is("3.00")
        .await;
}

#[tokio::test]
async fn transfer_creates_provident_book_transfer() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_account_number_maps_to_account_id("account2", "account2_id")
        .when_i_transfer_from_to_accounts(
            TransferRequest::new(
                "account1",
                "account2",
                Usd::from_f64_major_units(5.12_f64).unwrap(),
            ),
            Some("some random metadata".to_string()),
        )
        .await
        .then_book_transfer_source_account_id_is("account1_id")
        .await
        .then_book_transfer_destination_account_id_is("account2_id")
        .await
        .then_book_transfer_amount_is("5.12")
        .await
        .then_book_transfer_metadata_is("some random metadata")
        .await
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn when_looking_up_acct_id_for_transfer_destination_unreachable_then_bank_returns_xand_bank_error(
) {
    TreasuryPrimeScenario::default()
        .given_account_summaries_for_account_number_throws_error(
            "account1",
            TreasuryPrimeError::TreasuryPrimeClientError {
                message: "server error!".to_string(),
            },
        )
        .when_i_transfer_from_to_accounts(
            TransferRequest::new(
                "account1",
                "account2",
                Usd::from_f64_major_units(5.00_f64).unwrap(),
            ),
            Some("some other metadata".to_string()),
        )
        .await
        .then_transfer_returned_error_message("TreasuryPrimeClientError: server error!")
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn when_looking_up_acct_id_for_transfer_source_unreachable_then_bank_returns_xand_bank_error()
{
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_account_summaries_for_account_number_throws_error(
            "account2",
            TreasuryPrimeError::TreasuryPrimeClientError {
                message: "server error!".to_string(),
            },
        )
        .when_i_transfer_from_to_accounts(
            TransferRequest::new(
                "account1",
                "account2",
                Usd::from_f64_major_units(5.00_f64).unwrap(),
            ),
            Some("some special metadata".to_string()),
        )
        .await
        .then_transfer_returned_error_message("TreasuryPrimeClientError: server error!")
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn when_transferring_and_destination_account_not_found_then_error() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("source_acct", "source_id")
        .when_i_transfer_from_to_accounts(
            TransferRequest::new(
                "source_acct",
                "destination_acct",
                Usd::from_f64_major_units(5.00_f64).unwrap(),
            ),
            Some("some interesting metadata".to_string()),
        )
        .await
        .then_transfer_returned_error_message(
            "Unable to find account. Account number: destination_acct",
        )
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn when_transfering_and_source_account_not_found_then_error() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("destination_acct", "destination_id")
        .when_i_transfer_from_to_accounts(
            TransferRequest::new(
                "source_acct",
                "destination_acct",
                Usd::from_f64_major_units(5.00_f64).unwrap(),
            ),
            Some("some nifty metadata".to_string()),
        )
        .await
        .then_transfer_returned_error_message("Unable to find account. Account number: source_acct")
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn when_creating_transfer_and_multiple_destination_accounts_are_found_then_error() {
    TreasuryPrimeScenario::default()
        .given_two_accounts_with_same_account_number("source_acct")
        .given_account_number_maps_to_account_id("destination_acct", "destination_id")
        .when_i_transfer_from_to_accounts(
            TransferRequest::new(
                "source_acct",
                "destination_acct",
                Usd::from_f64_major_units(5.00_f64).unwrap(),
            ),
            Some("some morose metadata".to_string()),
        )
        .await
        .then_transfer_returned_error_message(
            "Retrieved more than one account. Account number: source_acct",
        )
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn when_creating_transfer_and_multiple_source_accounts_are_found_then_error() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("source_acct", "source_id")
        .given_two_accounts_with_same_account_number("destination_acct")
        .when_i_transfer_from_to_accounts(
            TransferRequest::new(
                "source_acct",
                "destination_acct",
                Usd::from_f64_major_units(5.00_f64).unwrap(),
            ),
            Some("some happy metadata".to_string()),
        )
        .await
        .then_transfer_returned_error_message(
            "Retrieved more than one account. Account number: destination_acct",
        )
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn transfer_returns_error_when_creating_book_transfer_returns_server_error() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_account_number_maps_to_account_id("account2", "account2_id")
        .given_create_book_transfer_request_returns(TreasuryPrimeError::TreasuryPrimeClientError {
            message: "some server error!".to_string(),
        })
        .when_i_transfer_from_to_accounts(
            TransferRequest::new(
                "account1",
                "account2",
                Usd::from_f64_major_units(5.00_f64).unwrap(),
            ),
            Some("some varied metadata".to_string()),
        )
        .await
        .then_transfer_returned_error_message("TreasuryPrimeClientError: some server error!")
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
#[should_panic]
async fn transfer_fails_when_amt_has_fractional_minor_units() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_account_number_maps_to_account_id("account2", "account2_id")
        .when_i_transfer_from_to_accounts(
            TransferRequest::new(
                "account1",
                "account2",
                Usd::from_f64_major_units(10.005_f64).unwrap(),
            ),
            Some("some random metadata".to_string()),
        )
        .await
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_constructs_correctly_from_transfer() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer(
            "account1_id",
            "account2_id",
            "transfer_123",
            "1.56",
            Some("meta123"),
            SENT_STATUS,
        )
        .await
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_i_get_a_transaction_item("meta123", Usd::from_f64_major_units(1.56).unwrap(), "Debit")
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_includes_time_from_date_range_when_calling_api() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer(
            "account1_id",
            "account2_id",
            "transfer_123",
            "1.56",
            Some("meta123"),
            SENT_STATUS,
        )
        .await
        .when_i_get_history_for("account1", "2020-01-04T12:34:56Z", "2020-01-05T01:23:45Z")
        .await
        .then_book_transfers_api_was_called_ntimes_with(vec![GetBookTransferRequest {
            from_date: "2020-01-04T12:34:56Z".to_string(),
            to_date: "2020-01-05T01:23:45Z".to_string(),
            page_cursor: None,
        }])
        .await
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_returns_credit_transaction_type_if_account_received_money() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account2", "account2_id")
        .given_book_transfer(
            "account1_id",
            "account2_id",
            "transfer_123",
            "1.56",
            Some("meta123"),
            SENT_STATUS,
        )
        .await
        .when_i_get_history_for("account2", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_i_get_a_transaction_item(
            "meta123",
            Usd::from_f64_major_units(1.56).unwrap(),
            "Credit",
        )
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_returns_debit_transaction_type_if_account_sent_money() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer(
            "account1_id",
            "account2_id",
            "transfer_123",
            "1.56",
            Some("meta123"),
            SENT_STATUS,
        )
        .await
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_i_get_a_transaction_item("meta123", Usd::from_f64_major_units(1.56).unwrap(), "Debit")
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_ignores_transactions_without_metadata() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer(
            "account1_id",
            "account2_id",
            "transfer_123",
            "1.56",
            None,
            SENT_STATUS,
        )
        .await
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_history_txn_count_is(0)
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_transaction_doesnt_match_source_or_destination_account() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer(
            "account3_id",
            "account2_id",
            "transfer_123",
            "1.56",
            Some("meta123"),
            SENT_STATUS,
        )
        .await
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_history_txn_count_is(0)
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_transaction_with_duplicate_source_and_destination_accounts_ignored() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer(
            "account1_id",
            "account1_id",
            "transfer_123",
            "1.56",
            Some("meta123"),
            SENT_STATUS,
        )
        .await
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_history_txn_count_is(0)
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_transaction_with_blank_status_ignored() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer(
            "account1_id",
            "account2_id",
            "transfer_123",
            "1.56",
            Some("meta123"),
            "",
        )
        .await
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_history_txn_count_is(0)
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_transaction_with_other_status_ignored() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer(
            "account1_id",
            "account2_id",
            "transfer_123",
            "1.56",
            Some("meta123"),
            "some other status",
        )
        .await
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_history_txn_count_is(0)
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_returns_error_then_adapter_returns_error() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer_returns_error(TreasuryPrimeClientError {
            message: "some error?!".to_string(),
        })
        .await
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_history_returned_error_message("TreasuryPrimeClientError: some error?!")
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_when_account_id_not_found_then_error() {
    TreasuryPrimeScenario::default()
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_history_returned_error_message("Unable to find account. Account number: account1")
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_when_amount_cannot_be_parsed() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer(
            "account1_id",
            "account2_id",
            "transfer_123",
            "hello",
            Some("meta123"),
            SENT_STATUS,
        )
        .await
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_history_returned_error()
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_when_userdata_is_none_then_book_transfer_ignored() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer_for_account_id_but_without_userdata("account1")
        .await
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_history_txn_count_is(0)
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_when_results_are_paged_then_all_pages_transactions_are_returned() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer_page(BookTransferPage {
            book_transfers: vec![Transfer {
                from_account_id: "account1_id".to_string(),
                to_account_id: "account2_id".to_string(),
                transfer_id: "transfer_123".to_string(),
                amount: "1.56".to_string(),
                status: SENT_STATUS.to_string(),
                meta_data: Some("metadata123".to_string()),
            }],
            next_page: Some("https://blah.com/book?page_cursor=book_123".to_string()),
        })
        .await
        .given_book_transfer_page(BookTransferPage {
            book_transfers: vec![Transfer {
                from_account_id: "account2_id".to_string(),
                to_account_id: "account1_id".to_string(),
                transfer_id: "transfer_456".to_string(),
                amount: "2.78".to_string(),
                status: SENT_STATUS.to_string(),
                meta_data: Some("metadata456".to_string()),
            }],
            next_page: None,
        })
        .await
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_history_txn_count_is(2)
        .then_i_get_a_transaction_item(
            "metadata123",
            Usd::from_f64_major_units(1.56).unwrap(),
            "Debit",
        )
        .then_i_get_a_transaction_item(
            "metadata456",
            Usd::from_f64_major_units(2.78).unwrap(),
            "Credit",
        )
        .then_book_transfers_api_was_called_ntimes_with(vec![
            GetBookTransferRequest {
                from_date: "2020-01-04T10:39:57Z".to_string(),
                to_date: "2020-01-05T10:39:57Z".to_string(),
                page_cursor: None,
            },
            GetBookTransferRequest {
                from_date: "2020-01-04T10:39:57Z".to_string(),
                to_date: "2020-01-05T10:39:57Z".to_string(),
                page_cursor: Some("book_123".to_string()),
            },
        ])
        .await
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_returns_url_parse_error_when_next_page_malformed() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer_page(BookTransferPage {
            book_transfers: vec![Transfer {
                from_account_id: "account1_id".to_string(),
                to_account_id: "account2_id".to_string(),
                transfer_id: "transfer_123".to_string(),
                amount: "1.56".to_string(),
                status: SENT_STATUS.to_string(),
                meta_data: Some("metadata123".to_string()),
            }],
            next_page: Some("klkajsdfkjlkjasdf".to_string()),
        })
        .await
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_history_returned_error_message(
            "Unable to parse next page cursor URL: klkajsdfkjlkjasdf",
        )
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn history_returns_url_parse_error_when_cursor_query_parameter_is_missing() {
    TreasuryPrimeScenario::default()
        .given_account_number_maps_to_account_id("account1", "account1_id")
        .given_book_transfer_page(BookTransferPage {
            book_transfers: vec![Transfer {
                from_account_id: "account1_id".to_string(),
                to_account_id: "account2_id".to_string(),
                transfer_id: "transfer_123".to_string(),
                amount: "1.56".to_string(),
                status: SENT_STATUS.to_string(),
                meta_data: Some("metadata123".to_string()),
            }],
            next_page: Some("https://blah.com/book?bad_name=book_123".to_string()),
        })
        .await
        .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
        .await
        .then_history_returned_error_message(
            "Unable to parse next page cursor URL: https://blah.com/book?bad_name=book_123",
        )
        .then_all_requests_authenticated_with_at_least_one_request(
            &get_default_client_authentication(),
        )
        .await;
}

#[tokio::test]
async fn two_requests_changed_authentication_updates() {
    let new_username = "new_username_value";
    let new_password = "new_password_value";
    let new_auth = get_basic_http_client_authentication(new_username, new_password);
    TreasuryPrimeScenario::default()
        .given_account_number_with_balances("account1", "5.04", "5.05")
        .when_i_get_balance_for_account("account1")
        .await
        .when_i_change_my_basic_http_authentication(new_username, new_password)
        .await
        .when_i_get_balance_for_account("account1")
        .await
        .then_there_are_n_authenticated_requests(4)
        .await
        .then_the_ith_request_is_authenticated(0, &get_default_client_authentication())
        .await
        .then_the_ith_request_is_authenticated(1, &get_default_client_authentication())
        .await
        .then_the_ith_request_is_authenticated(2, &new_auth)
        .await
        .then_the_ith_request_is_authenticated(3, &new_auth)
        .await;
}

#[tokio::test]
async fn nonexistent_key_error() {
    let secret_store = FakeSecretStore::default();
    TreasuryPrimeScenario::default_from_secret_store(secret_store)
        .given_account_number_with_balances("account1", "5.04", "5.05")
        .when_i_get_balance_for_account("account1")
        .await
        .then_balance_returned_error_message("Secret store lookup error: the key \"fake/secrets/path/api_key_id\" was not found within the secret store");
}
