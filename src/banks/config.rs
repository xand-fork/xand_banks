use crate::banks::adapter_config::AdapterConfig;
use crate::models::account::Account;
use crate::{
    banks::XandBank, logging_events::LoggingEvent, metrics::MeteredBankDecorator, Bank,
    BankAdapter, BanksConfig, Result,
};
use regex::Regex;
use snafu::Snafu;
use std::{collections::HashMap, sync::Arc};
use xand_secrets::SecretKeyValueStore;

#[derive(Debug, Deserialize, Serialize, Clone, Default, PartialEq, Eq)]
pub struct XandBanksConfig {
    #[serde(default)]
    pub banks: Vec<BankConfig>,
}

impl XandBanksConfig {
    /// Validates that the bank configurations contained within are sensible. Namely:
    ///
    ///  * Prints warning if there are no defined banks
    ///  * Verifies distinct banks do not have duplicate routing numbers
    ///  * Bubbles up result of `validate()` on each bank
    pub fn validate(&self) -> Result<()> {
        if self.banks.is_empty() {
            warn!(LoggingEvent::Msg(
                "No banks are provided in this configuration.".to_string()
            ));
        } else {
            self.check_for_duplicate_routing_numbers()?;
            for bank in &self.banks {
                bank.validate()?;
            }
        }

        Ok(())
    }

    fn check_for_duplicate_routing_numbers(&self) -> Result<()> {
        let mut seen_routing_numbers = vec![];
        for bank in &self.banks {
            if seen_routing_numbers.contains(&bank.routing_number) {
                return Err(BankConfigurationErrors::DuplicateRoutingNumberError {
                    routing_number: bank.routing_number.clone(),
                }
                .into());
            } else {
                seen_routing_numbers.push(bank.routing_number.clone())
            }
        }

        Ok(())
    }
}

impl BanksConfig for XandBanksConfig {
    /// Create a map of routing_number -> Bank for each available bank.
    fn get_banks(
        &self,
        secret_store: Arc<dyn SecretKeyValueStore>,
    ) -> Result<HashMap<String, Box<dyn Bank>>> {
        // This type annotation is needed for now until we add a second impl Bank
        let mut banks = MeteredBanksMap::default();

        for bank_config in self.banks.clone() {
            let mut accounts = HashMap::new();
            for account in bank_config.accounts.clone() {
                accounts.insert(account.id.to_string(), account.clone());
            }
            let adapter = bank_config.create_adapter(secret_store.clone())?;

            let bank = XandBank {
                name: bank_config.name.clone(),
                routing_number: bank_config.routing_number.clone(),
                trust_account: Account {
                    id: "trust".to_string(),
                    short_name: "trust account".to_string(),
                    account_number: bank_config.trust_account,
                },
                adapter,
                accounts,
            };

            banks.insert(bank_config.routing_number.clone(), bank);
        }

        Ok(banks.into_map())
    }
}

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq, Eq)]
#[serde(rename_all = "kebab-case")]
pub struct BankConfig {
    pub name: String,
    pub routing_number: String,
    pub trust_account: String,
    #[serde(default)]
    pub accounts: Vec<Account>,
    pub adapter: AdapterConfig,
}

impl BankConfig {
    fn check_for_duplicate_account_numbers(&self) -> Result<()> {
        let mut seen_account_numbers = vec![];
        for account in &self.accounts {
            if seen_account_numbers.contains(&account.account_number) {
                return Err(BankConfigurationErrors::DuplicateAccountNumberError {
                    bank_name: self.name.clone(),
                    account_number: account.account_number.clone(),
                }
                .into());
            } else {
                seen_account_numbers.push(account.account_number.clone())
            }
        }

        Ok(())
    }

    pub fn create_adapter(
        &self,
        secret_store: Arc<dyn SecretKeyValueStore>,
    ) -> Result<Box<dyn BankAdapter>> {
        self.adapter.create(secret_store)
    }

    pub fn validate(&self) -> Result<()> {
        if self.name.trim().is_empty() {
            return Err(BankConfigurationErrors::GenericConfigError {
                msg: "Bank name must be set".to_string(),
            }
            .into());
        }
        let nine_digit_number = Regex::new(r"^\d{9}$").unwrap();
        if !nine_digit_number.is_match(&self.routing_number) {
            return Err(BankConfigurationErrors::GenericConfigError {
                msg: format!(
                    "{} routing number is not ABA format: '{}'",
                    &self.name, &self.routing_number
                ),
            }
            .into());
        }

        self.check_for_duplicate_account_numbers()?;

        self.adapter.validate()?;
        Ok(())
    }
}

#[derive(Default)]
struct MeteredBanksMap {
    banks: HashMap<String, Box<dyn Bank>>,
}

impl MeteredBanksMap {
    fn insert<T: 'static + Bank + Clone + Send + Sync>(&mut self, key: String, bank: T) {
        let bank = Box::new(MeteredBankDecorator::new(bank));
        self.banks.insert(key, bank);
    }
    fn into_map(self) -> HashMap<String, Box<dyn Bank>> {
        self.banks
    }
}

#[derive(Debug, Snafu, Clone, Serialize)]
#[snafu(visibility(pub(crate)))]
pub enum BankConfigurationErrors {
    #[snafu(display("Configuration issue: {}", msg))]
    GenericConfigError { msg: String },
    #[snafu(display("Underlying IO error ({}): {:?}", msg, source))]
    UnderlyingIoError {
        msg: String,
        #[snafu(source(from(std::io::Error, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<std::io::Error>,
    },
    #[snafu(display("Error decoding bank cert: {:?}", source))]
    BankCertDecodeProblem {
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: base64::DecodeError,
    },
    #[snafu(display("Format error: {}", msg))]
    FormattingError { msg: String },
    #[snafu(display("Duplicate routing number: {}", routing_number))]
    DuplicateRoutingNumberError { routing_number: String },
    #[snafu(display(
        "{} configuration contains duplicate account numbers: {}",
        bank_name,
        account_number
    ))]
    DuplicateAccountNumberError {
        bank_name: String,
        account_number: String,
    },
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::banks::treasury_prime_adapter::config::TreasuryPrimeAuthConfig;
    use crate::banks::treasury_prime_adapter::config::TreasuryPrimeConfig;
    use crate::constants::BANK_CALL_STANDARD_TIMEOUT;
    use crate::models::account::Account;
    use url::Url;

    fn build_treasury_prime_bank(
        name: &str,
        routing_number: &str,
        accounts: Vec<Account>,
    ) -> BankConfig {
        let provident_adapter = TreasuryPrimeConfig {
            url: Url::parse("http://fake_url:1986").unwrap(),
            auth: TreasuryPrimeAuthConfig {
                secret_key_username: String::from("barneys/file/path/barney_username"),
                secret_key_password: String::from("barneys/file/path/barney_password"),
            },
            timeout: BANK_CALL_STANDARD_TIMEOUT,
        };
        BankConfig {
            name: name.to_string(),
            routing_number: routing_number.to_string(),
            trust_account: "123456789".to_string(),
            accounts,
            adapter: AdapterConfig::TreasuryPrime(provident_adapter),
        }
    }

    #[test]
    fn no_banks_are_okay() {
        let no_banks_config = XandBanksConfig { banks: vec![] };

        let res = no_banks_config.validate();

        assert!(res.is_ok());
    }

    #[test]
    fn nine_digit_routing_number_is_valid() {
        let cfg = build_treasury_prime_bank("Bill and Ted's Excellent Bank", "999666333", vec![]);

        assert!(cfg.validate().is_ok());
    }

    #[test]
    fn eight_digit_routing_number_is_error() {
        let cfg = build_treasury_prime_bank("Bill and Ted's Excellent Bank", "99966633", vec![]);

        let result = cfg.validate();
        assert!(result.is_err(), "{:?}", result);

        assert_eq!(
            "Configuration issue: Bill and Ted's Excellent Bank routing number is not ABA format: '99966633'",
            result.unwrap_err().to_string(),
        );
    }

    #[test]
    fn nine_digit_routing_number_with_padding_is_error() {
        let cfg = build_treasury_prime_bank("Bill and Ted's Excellent Bank", " 999666333 ", vec![]);

        let result = cfg.validate();
        assert!(result.is_err(), "{:?}", result);

        assert_eq!(
            "Configuration issue: Bill and Ted's Excellent Bank routing number is not ABA format: ' 999666333 '",
            result.unwrap_err().to_string(),
        );
    }

    #[test]
    fn bank_must_have_name() {
        let cfg = build_treasury_prime_bank("", "99966633", vec![]);

        let result = cfg.validate();
        assert!(result.is_err(), "{:?}", result);

        assert_eq!(
            result.unwrap_err().to_string(),
            "Configuration issue: Bank name must be set",
        );
    }

    #[test]
    fn bank_name_cannot_be_white_space() {
        let cfg = build_treasury_prime_bank("\t  ", "99966633", vec![]);

        let result = cfg.validate();
        assert!(result.is_err(), "{:?}", result);

        assert_eq!(
            result.unwrap_err().to_string(),
            "Configuration issue: Bank name must be set",
        );
    }

    #[test]
    fn no_duplicate_routing_numbers() {
        let joe_cfg = build_treasury_prime_bank("Joe Exotic's Bank of Tigers", "999666333", vec![]);
        let carole_cfg =
            build_treasury_prime_bank("Carole Baskin's Credit Union", "999666333", vec![]);

        let banks_cfg = XandBanksConfig {
            banks: vec![joe_cfg, carole_cfg],
        };
        let result = banks_cfg.validate();
        assert!(result.is_err(), "{:?}", result);

        assert_eq!(
            result.unwrap_err().to_string(),
            "Duplicate routing number: 999666333"
        );
    }

    #[test]
    fn no_duplicate_account_numbers_at_the_same_bank() {
        let cfg = build_treasury_prime_bank(
            "Joe Exotic's Bank of Tigers",
            "999666333",
            vec![
                Account {
                    id: "joe".to_string(),
                    short_name: "Joe Exotic's Bank Account".to_string(),
                    account_number: "666".to_string(),
                },
                Account {
                    id: "carole".to_string(),
                    short_name: "Carole Baskin's Bank Account".to_string(),
                    account_number: "666".to_string(),
                },
            ],
        );

        let result = cfg.validate();
        assert!(result.is_err(), "{:?}", result);

        assert_eq!(
            result.unwrap_err().to_string(),
            "Joe Exotic's Bank of Tigers configuration contains duplicate account numbers: 666"
        );
    }

    #[test]
    fn duplicate_accounts_allowed_at_different_banks() {
        let joe_cfg = build_treasury_prime_bank(
            "Joe Exotic's Bank of Tigers",
            "999666333",
            vec![Account {
                id: "joe".to_string(),
                short_name: "Joe Exotic's Bank Account".to_string(),
                account_number: "666".to_string(),
            }],
        );
        let carole_cfg = build_treasury_prime_bank(
            "Carole Baskin's Credit Union",
            "333666999",
            vec![Account {
                id: "carole".to_string(),
                short_name: "Carole Baskin's Bank Account".to_string(),
                account_number: "666".to_string(),
            }],
        );
        let cfg = XandBanksConfig {
            banks: vec![joe_cfg, carole_cfg],
        };

        let result = cfg.validate();
        assert!(result.is_ok(), "{:?}", result);
    }
}
