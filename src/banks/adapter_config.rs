use crate::banks::mcb_adapter::config::McbConfig;
use crate::banks::treasury_prime_adapter::config::TreasuryPrimeConfig;
use crate::BankAdapter;
use std::sync::Arc;
use xand_secrets::SecretKeyValueStore;

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq, Eq)]
#[serde(rename_all = "kebab-case")]
pub enum AdapterConfig {
    Mcb(McbConfig),
    TreasuryPrime(TreasuryPrimeConfig),
}

impl AdapterConfig {
    pub fn create(
        &self,
        secret_store: Arc<dyn SecretKeyValueStore>,
    ) -> crate::Result<Box<dyn BankAdapter>> {
        match self {
            AdapterConfig::Mcb(cfg) => Ok(Box::new(cfg.get_adapter(secret_store)?)),
            AdapterConfig::TreasuryPrime(cfg) => Ok(Box::new(cfg.get_adapter(secret_store)?)),
        }
    }

    pub fn validate(&self) -> crate::Result<()> {
        match self {
            AdapterConfig::Mcb(cfg) => cfg.validate(),
            AdapterConfig::TreasuryPrime(cfg) => cfg.validate(),
        }
    }
}
