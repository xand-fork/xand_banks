#![allow(non_snake_case)]
#[cfg(test)]
mod tests {

    pub mod bank_mocks_container;
    pub mod docker;
    pub mod fake_secrets;

    mod treasury_prime_tests {
        use crate::tests::bank_mocks_container::BankMocksContainer;
        use crate::tests::fake_secrets::FakeSecrets;
        use async_trait::async_trait;
        use std::sync::Arc;
        use url::Url;
        use xand_banks::banks::bank_certification_tests::{
            BankAdapterTestContext, BankAdapterTimeoutTestContext,
        };
        use xand_banks::banks::treasury_prime_adapter::client_wrapper::RealClient;
        use xand_banks::banks::treasury_prime_adapter::config::{
            TreasuryPrimeAuthConfig, TreasuryPrimeConfig,
        };
        use xand_banks::banks::treasury_prime_adapter::TreasuryPrimeAdapter;
        use xand_banks::{bank_adapter_tests, bank_adapter_timeout_tests};

        bank_adapter_tests!(TreasuryPrimeDockerBankAdapterTestContext::new().await);

        bank_adapter_timeout_tests!(TreasuryPrimeDockerBankAdapterTestContext::new().await);

        struct TreasuryPrimeDockerBankAdapterTestContext {
            // Hold this so it remains in scope for test. On drop, docker container is stopped
            bank_mocks: BankMocksContainer,
        }

        #[async_trait]
        impl BankAdapterTestContext for TreasuryPrimeDockerBankAdapterTestContext {
            type Adapter = TreasuryPrimeAdapter<RealClient>;

            async fn get_adapter(&self) -> TreasuryPrimeAdapter<RealClient> {
                self.build_treasury_prime_adapter_with(self.bank_mocks.treasury_prime_url(), 60)
            }
        }

        #[async_trait]
        impl BankAdapterTimeoutTestContext for TreasuryPrimeDockerBankAdapterTestContext {
            type Adapter = TreasuryPrimeAdapter<RealClient>;

            async fn get_adapter_with_timeout(&self, timeout_in_seconds: u64) -> Self::Adapter {
                self.build_treasury_prime_adapter_with(
                    self.bank_mocks.treasury_prime_url(),
                    timeout_in_seconds,
                )
            }

            async fn set_latency(&self, latency_in_seconds: u64) {
                self.bank_mocks.add_latency(latency_in_seconds).await;
            }
        }

        impl TreasuryPrimeDockerBankAdapterTestContext {
            async fn new() -> Self {
                let bank_mocks = BankMocksContainer::start_on_ephemeral()
                    .await
                    .expect("Failed to start bank mocks");
                bank_mocks.wait_until_healthy().await;

                TreasuryPrimeDockerBankAdapterTestContext { bank_mocks }
            }

            fn build_treasury_prime_adapter_with(
                &self,
                treasury_prime_url: Url,
                timeout: u64,
            ) -> TreasuryPrimeAdapter<RealClient> {
                let config = TreasuryPrimeConfig {
                    url: treasury_prime_url,
                    auth: TreasuryPrimeAuthConfig {
                        secret_key_username: String::from("DummyValue"),
                        secret_key_password: String::from("AnotherDummyValue"),
                    },
                    timeout,
                };
                TreasuryPrimeAdapter::new(&config, Arc::new(FakeSecrets))
                    .expect("Failed to create bank adapter")
            }
        }
    }

    mod mcb_tests {
        use crate::tests::bank_mocks_container::BankMocksContainer;
        use crate::tests::fake_secrets::FakeSecrets;
        use async_trait::async_trait;
        use std::sync::Arc;
        use url::Url;
        use xand_banks::banks::bank_certification_tests::{
            BankAdapterTestContext, BankAdapterTimeoutTestContext,
        };
        use xand_banks::banks::mcb_adapter::config::{
            McbClientTokenRetrievalAuthConfig, McbConfig, RawAccountInfoManagerConfig,
        };
        use xand_banks::banks::mcb_adapter::CloneableMcbAdapter;
        use xand_banks::{bank_adapter_tests, bank_adapter_timeout_tests};

        bank_adapter_tests!(McbDockerBankAdapterTestContext::new().await);

        bank_adapter_timeout_tests!(McbDockerBankAdapterTestContext::new().await);

        struct McbDockerBankAdapterTestContext {
            // Hold this so it remains in scope for test. On drop, docker container is stopped
            bank_mocks: BankMocksContainer,
        }

        #[async_trait]
        impl BankAdapterTestContext for McbDockerBankAdapterTestContext {
            type Adapter = CloneableMcbAdapter;

            async fn get_adapter(&self) -> CloneableMcbAdapter {
                self.build_mcb_adapter_with(self.bank_mocks.mcb_url(), 60)
            }
        }

        #[async_trait]
        impl BankAdapterTimeoutTestContext for McbDockerBankAdapterTestContext {
            type Adapter = CloneableMcbAdapter;

            async fn get_adapter_with_timeout(&self, timeout_in_seconds: u64) -> Self::Adapter {
                self.build_mcb_adapter_with(self.bank_mocks.mcb_url(), timeout_in_seconds)
            }

            async fn set_latency(&self, latency_in_seconds: u64) {
                self.bank_mocks.add_latency(latency_in_seconds).await;
            }
        }

        impl McbDockerBankAdapterTestContext {
            async fn new() -> Self {
                let bank_mocks = BankMocksContainer::start_on_ephemeral()
                    .await
                    .expect("Failed to start bank mocks");
                bank_mocks.wait_until_healthy().await;

                McbDockerBankAdapterTestContext { bank_mocks }
            }

            fn build_mcb_adapter_with(&self, mcb_url: Url, timeout: u64) -> CloneableMcbAdapter {
                let mcb_config = McbConfig {
                    url: mcb_url,
                    refresh_if_expiring_in_secs: None,
                    auth: McbClientTokenRetrievalAuthConfig {
                        secret_key_username: "my-fake-metropolitan-username".to_string(),
                        secret_key_password: "my-fake-metropolitan-password".to_string(),
                    },
                    account_info_manager: RawAccountInfoManagerConfig {
                        secret_key_client_app_ident: "client_app_ident".to_string(),
                        secret_key_organization_id: "organization_id".to_string(),
                    },
                    timeout,
                };
                CloneableMcbAdapter::new(&mcb_config, Arc::new(FakeSecrets))
                    .expect("Failed to create bank adapter")
            }
        }
    }
}
