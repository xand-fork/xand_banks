use bollard::Docker;
use futures::TryFutureExt;

pub mod error {
    use thiserror::Error;

    #[derive(Debug, Error)]
    pub enum Error {
        #[error(transparent)]
        Docker(#[from] bollard::errors::Error),
    }
}

pub async fn create_docker_connection() -> Result<Docker, error::Error> {
    try_http_defaults().or_else(|_| try_socket_defaults()).await
}

async fn try_http_defaults() -> Result<Docker, error::Error> {
    let docker = Docker::connect_with_http_defaults()?;
    docker.info().await?;
    Ok(docker)
}

async fn try_socket_defaults() -> Result<Docker, error::Error> {
    let docker = Docker::connect_with_socket_defaults()?;
    docker.info().await?;
    Ok(docker)
}
